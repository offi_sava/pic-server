
const loginator = require('loginator'),
    _ = require('lodash'),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    r = require('../lib/api-response'),
    ApiError = require('../lib/errors').ApiError,
    log = loginator.getLogger('server'),
    express = require('express'),
    config = require('../config');



module.exports = class Rest {

    static dependencies() {
        return ['context']
    }
    /**
     * @Initialize
     */
    initialize(callback) {
        this.app = express();
        let port = process.env.PORT || 3001;


        let server = require('http').createServer(this.app);
        //let io = require('socket.io')(server);
        loginator.configure(config.loginator);
        log.info("Initializing server...");

        server.listen(port, function(){
            log.info('Server listening in http://localhost:' + port);
        });

        this.app.use(bodyParser.json({strict: false}));
        this.app.use(function (error, req, res, next) {
            if (error instanceof SyntaxError) {
                res.json(r.errResponse(new ApiError('malformed_request', 'Wrong request format', error)));
            } else {
                next();
            }
        });

        callback();
    }

    async loadScopes(path) {
        try {
            let files = await fs.promises.readdir(path);
            for (let f of files) {
                if (f === 'index.js') continue;
                let scope = `/${_.toLower(f.split('.js')[0])}/`;
                this.app.use(scope, await require(path + '/' + f)(this.context));
                log.info('Loaded ' + scope + ' endpoints');
            }
        } catch (err) {
            log.error(err.stack);
        }

    }

    async prepare() {
        try {
            let router = express.Router();
            this.app.all('*', require('../lib/logging').accessLogFilter);
            this.app.all('*', require('../lib/allowcrossdomain'));
            this.app.all('/', (req, res) => {
                res.json(r.okResponse({metaInfo:'jkk'}));
            });
            await this.loadScopes(__dirname + '/endpoints/');
            /* Catch request syntax errors(when sent wrong JSON format) */
            router.use(function (error, req, res, next) {
                if (error.stack) {
                    log.error(error, error.stack);
                }
                res.json(r.errResponse(new ApiError('problem', error.message || 'Something went wrong', error)));
            });
        } catch (e) {
            log.error(e);
        }
    }

};

