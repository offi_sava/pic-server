'use strict';

const _ =  require('lodash'),
    router = require('express').Router(),
    log = require('loginator').getLogger('rest/catchwords'),
    r = require('../../lib/api-response'),
    fs = require('fs'),
    ApiError = require('../../lib/errors').ApiError;
//const openApiFile = __dirname + '/s/words.json'

module.exports = async function (context) {


    router.get('/',  async (req, res) => {
        try {
            res.json(r.okResponse(_.shuffle([
                'Яблоко',
                'Санки',
                'Автобус',
                'Птица',
                'Доска',
                'Телевизор',
                'Дождь',
                'Велосипед',
                'Трава',
                'Математика',
                'Мыло',
                'Рукавица',
                'Снег',
                'Учебник',
                'Остановка',
                'Кукушка',
                'Мультфильм',
                'Елка',
                'Вода',
                'Рассвет',
                'Звонок',
                'Море',
                'Чай',
                'Бабочка',
                'Компьютер',
                'Море',
                'Зарплата',
                'Леденец',
                'Улитка',
                'Белка',
                'Облако',
                'Змея'
            ])));
        } catch (err) {
            log.error(err);
            res.json(r.errResponse(err));
        }
    });




    //
    // router.get('/', (req, res) => {
    //     fs.readFile(openApiFile, function (err, docFile) {
    //         if (err) throw err;
    //         let fileContent = docFile && docFile.length ? JSON.parse(docFile) : [];
    //
    //
    // });





    return router;
}
