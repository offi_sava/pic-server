/**
 *
 * Forms endpoint
 *
 * @author Alexandra Riabova
 */

'use strict';

const router = require('express').Router(),
    log = require('loginator').getLogger('rest/rooms'),
    r = require('../../lib/api-response'),
    ApiError = require('../../lib/errors').ApiError;

module.exports = async function (context) {

    const service = context.getBean('room');
   // const db = context.getBean('db');

    router.get('/',async (req, res) => {
        try {
            log.info(req.query);
            let rooms = req.query.userId ? await service.getRoomsByUserId(req.query.userId) : await service.getRooms();
            log.info(rooms)
            res.json(r.okResponse(rooms));
        } catch (err) {
            log.error(err);
            res.json(r.errResponse(err));
        }
    });

    router.get('/:id',  async (req, res) => {
        try {
            let data = req.query.userId ? await service.getOtherRooms(req.query.userId) : await service.getRoomById(req.params.id);
            log.info(data)
            res.json(r.okResponse(data));
        } catch (err) {
            log.error(err);
            res.json(r.errResponse(err));
        }
    });

    router.get('/others',  async (req, res) => {
        try {
            log.info(req.query);
            let rooms = await service.getOtherRooms(req.query.userId);
            log.info(rooms)
            res.json(r.okResponse(rooms));
        } catch (err) {
            log.error(err);
            log.error('kkkkkk');
            res.json(r.errResponse(err));
        }
    });



    router.post('/',  async (req, res) => {
        try {
            log.info(req.body)
            log.info(req.query)
            let room = await service.createRoom(req.body);
            log.info(room);
            res.json(r.okResponse(room));
        } catch (err) {
            log.error(err);
            res.json(r.errResponse(err));
        }
    });

    router.delete('/',  async (req, res) => {
        try {
            log.info(req.body)
            log.info(req.query)
            let data = await service.deleteRoom(req.query);
            log.info(data);
            res.json(r.okResponse(data));
        } catch (err) {
            log.error(err);
            res.json(r.errResponse(err));
        }
    });


    router.post('/op',  async (req, res) => {
        try {

            const point = { type: 'Point', coordinates: [39.807222,-76.984722]};
            let room = await db.User.create({point, entityIds: [{ll: 'oo'}]})
            log.info(room);
            res.json(r.okResponse(room));
        } catch (err) {
            log.info(err);
            res.json(r.errResponse(err));
        }
    });


    router.put('/:id',  async (req, res) => {
        try {
            let rooms = await service.addNewUserToRoom(req.params.id, req.body);
            log.info(rooms)
            res.json(r.okResponse(rooms));
        } catch (err) {
            log.error(err);
            res.json(r.errResponse(err));
        }
    });



    router.post('/:id/left-room',  async (req, res) => {
        try {
            let rooms = await service.leftRoom(req.params.id, req.body);
            log.info(rooms)
            res.json(r.okResponse(rooms));
        } catch (err) {
            log.error(err);
            res.json(r.errResponse(err));
        }
    });



    return router;

};
