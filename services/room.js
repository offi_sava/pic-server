'use strict';

const redis = require('redis'),
    config = require('../config'),
    Promise = require('bluebird'),
    _ = require('lodash'),
    log = require('loginator').getLogger('services/room'),
    ApiError = require('../lib/errors').ApiError;

const ROOM_TOKEN_PREFIX = 'room_token::',
    USER_TOKEN_PREFIX = 'user_token::';

module.exports = class Room {

    static dependencies() {
        return ['cache']
    }
    /**
     * Initialize
     */
    initialize(callback) {

        log.info('Initializing...');
        callback();
    }

    async createRoom(opts) {
        if (!opts.userId && !opts.userName && !opts.roomName && !opts.roomId) {
            throw new ApiError('problem', 'Need userId, roomName, roomId');
        }
        let cachedData = await this.cache.get(USER_TOKEN_PREFIX + opts.userId);
        let r = [];
        if (cachedData) {
            r = cachedData.rooms;
        }

        r.push({roomName: opts.roomName, roomId: opts.roomId, isAdmin: true});

        await this.cache.set(USER_TOKEN_PREFIX + opts.userId, {rooms:r}, 10000000000);
        await this.cache.set(ROOM_TOKEN_PREFIX + opts.roomId, {
            roomId: opts.roomId,
            roomName: opts.roomName,
            masterId: opts.userId,
            users:[{userId: opts.userId, userName: opts.userName, isAdmin: true, isMaster: true}]
        }, 10000000000);
        return r;
    }



    async deleteRoom(opts) {
        if (!opts && !opts.roomId && !opts.userId) return;

        let userRooms = await this.cache.get(USER_TOKEN_PREFIX + opts.userId);

        log.info(userRooms);
        if (!userRooms) return;

        await this.cache.set(
            USER_TOKEN_PREFIX + opts.userId,
            {
                rooms: _.remove(userRooms.rooms, (r) => {
                    return r.roomId === opts.roomId
                })
            },
            10000000000
        );
        if (_.get(_.find(userRooms.rooms, ['roomId', opts.roomId]), 'isAdmin')) {
            log.info('llllllllll')
            await this.cache.del(ROOM_TOKEN_PREFIX + opts.roomId);
        }

        return _.get(userRooms, 'rooms');
    }


    async getRoomsByUserId(userId) {
        let data = await this.cache.get(USER_TOKEN_PREFIX + userId);
        log.info(data)
        return _.get(data, 'rooms', []);
    }

    async getOtherRooms(userId) {
        let rooms = [];
        let allRooms = await this.cache.list(ROOM_TOKEN_PREFIX + '*');

        if (!allRooms) return;

        let userData = await this.cache.get(USER_TOKEN_PREFIX + userId);

        log.info(userData)

        for (let key of allRooms) {
            let value = await this.cache.get(key);

            if (!_.get(userData, 'rooms') || !_.find(userData.rooms, ['roomId', value.roomId])) {
                rooms.push(value);
            }
        }
        return rooms;
    }

    async getRooms() {
        let rooms = [];
        let data = await this.cache.list(ROOM_TOKEN_PREFIX + '*');
        for (let key of data) {
            let value = await this.cache.get(key);
            rooms.push(value);
        }
        log.info(rooms)
        return rooms;
    }


    async addNewUserToRoom(id, opts) {
        if (!opts && !id && !opts.userId){
            log.info('no info');
            return;
        }
        let dataOfRoom = await this.cache.get(ROOM_TOKEN_PREFIX + id);

        if (!dataOfRoom) return;

        if (!_.find(dataOfRoom.users, ['userId', opts.userId])) {
            log.info('llllllllll')
            dataOfRoom.users.push({userId: opts.userId, userName: opts.userName})
        }

        //let roomsArr = await this.getRoomsByUserId(opts.userId);
        //roomsArr.push({roomName: dataOfRoom.roomName, roomId: dataOfRoom.roomId, isAdmin: false});
       // await this.cache.set(USER_TOKEN_PREFIX + opts.userId, {rooms:roomsArr}, 10000000000);
        await this.cache.set(ROOM_TOKEN_PREFIX + dataOfRoom.roomId, dataOfRoom, 10000000000);
        return dataOfRoom;
    }


    async getRoomById(id) {
        let dataOfRoom = await this.cache.get(ROOM_TOKEN_PREFIX + id);
        if (!dataOfRoom) throw new ApiError('object', 'Room not found');
        return dataOfRoom;
    }


    async leftRoom(id, opts) {
        if (!opts && !id && !opts.userId){
            log.info('no info');
            return;
        }
        let dataOfRoom = await this.cache.get(ROOM_TOKEN_PREFIX + id);

        if (!dataOfRoom) return;

        dataOfRoom.users = _.filter( dataOfRoom.users, u => {return u.userId !== opts.userId});

        await this.cache.set(ROOM_TOKEN_PREFIX + id, dataOfRoom, 10000000000);
        return dataOfRoom;
    }



    async joinRoom(userId, roomId)  {

        let cachedRoomData = await this.cache.get(ROOM_TOKEN_PREFIX + roomId);
        if (!cachedRoomData) throw new ApiError('object', 'Room not found');

        let r = [];
        let cachedUserData = await this.cache.get(USER_TOKEN_PREFIX + userId);

        if (cachedUserData) r = cachedUserData.rooms;

        r.push({roomName: opts.roomName, roomId: opts.roomId});

        cachedRoomData.users.push({userId: opts.userId, userName: opts.userName});

        await this.cache.set(USER_TOKEN_PREFIX + opts.userId, {rooms:r}, config.users.ttl);
        await this.cache.set(ROOM_TOKEN_PREFIX + opts.roomId, {
            roomId: opts.roomId,
            roomName: opts.roomName,
            users: cachedRoomData.users
        }, config.rooms.ttl);

    }

    async selectRandomMasterInRoom({oldMasterId, roomId}) {
        log.error(oldMasterId, roomId)
        let cachedRoomData = await this.cache.get(ROOM_TOKEN_PREFIX + roomId);
        if (!cachedRoomData) {
            log.info('object_not_found', 'Room not found');
            throw new ApiError('object_not_found', 'Room ' + roomId + ' not found');
        }
        if (cachedRoomData.users.length === 1) {
            log.info('problem', 'There is only one player in the room');
            throw new ApiError('object', 'There is only one player in the room ' + roomId);
        }
        cachedRoomData.users = _.orderBy(cachedRoomData.users, ['isMaster', true]);

        _.unset(cachedRoomData.users[0], 'isMaster');
        let randomId = _.random(1, cachedRoomData.users.length-1);

        _.set(cachedRoomData.users[randomId], 'isMaster', true);
        log.error(randomId, cachedRoomData.users)
        await this.cache.set(ROOM_TOKEN_PREFIX + roomId, {
            roomId: cachedRoomData.roomId,
            roomName: cachedRoomData.roomName,
            masterId: cachedRoomData.users[randomId].userId,
            users: cachedRoomData.users
        }, config.rooms.ttl);
        return {newMasterId: cachedRoomData.users[randomId].userId, roomId: roomId};


    }


};


