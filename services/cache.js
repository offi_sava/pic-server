'use strict';

const redis = require('async-redis'),
    config = require('../config'),
    log = require('loginator').getLogger('services/cache'),
    ApiError = require('../lib/errors').ApiError;


module.exports = class Cache {

    /**
     * Initialize
     */
    initialize(callback) {

        log.info('Initializing...');

        this.ttl = 3600;

        this.client = redis.createClient(
            config.redis.port,
            config.redis.host,
            config.redis.options
        );

        this.client.once('ready', () => {
            callback();
            this.client.removeAllListeners('error');
        });

        this.client.once('error', (err) => {
            callback(err);
            this.client.removeAllListeners('ready');
        });

    }

    /**
     * Get value by key
     */
    async get(key) {
        let val;
        try {
            val = await this.client.get(key);
        } catch (err) {
            log.error(err);
            throw err;
        }
        try {
            val = JSON.parse(val);
        } catch (e) {
            return new ApiError('problem','Invalid data format associated with this key. Please don\'t use it in future');
        }
        return val;

    }

    /**
     * Set or update value by key with ttl
     */
    async set(key, value, ttl) {
        ttl = ttl || this.ttl;
        value = JSON.stringify(value);
        return this.client.setex(key, ttl, value);
    }

    /**
     * Delete value by key
     */
    async del(key) {
        return this.client.del(key);
    }

    /**
     * Tough (update ttl) of value by key
     */
    async touch(key, ttl) {
        return this.client.expire(key, ttl || this.ttl);
    }

    /**
     * Find list of elements matching key
     */
    async list(key) {
        return this.client.keys(key);
    }

};


