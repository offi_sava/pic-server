/**
 * Database service
 *
 * @author Ievgen Ponomarenko
 */

'use strict';

let _ = require('lodash'),
     util = require('util'),
    fs = require('fs'),
    path = require('path'),
    cls = require('cls-hooked'),
    namespace = cls.createNamespace('transaction-namespace'),
    sequelize = require('sequelize'),
    log = require('loginator').getLogger('services/db');

let config = require('../config'),
    modelPath = path.resolve(__dirname + '/../models');


sequelize.useCLS(namespace);

let activeConnections = 0;

let orm = new sequelize(config.database.connectionString, {

    pool: config.database.pool,

    logging: function(str, i) {
        if (i.bind) {
            log.info(i.bind);
        }
        log.info(str);
    },

    hooks: {
        afterConnect: function (connection) {
            log.trace("new connection to db");
            activeConnections++;
        },
        afterDisconnect: function (connection) {
            log.trace("connection to db closed");
            activeConnections--;
        }
    }
});



module.exports = class Db {

    constructor() {
        this.orm = orm;
        this.sequelize = sequelize;
    }

    /**
     * Initialize
     */
    initialize(callback) {

        log.info("Initializing...");



        /* loading ORM models */
        _.each(fs.readdirSync(modelPath), (file) => {
            // skip dirs
            if (!fs.statSync(modelPath + '/' + file).isFile()) {
                return;
            }

            this[_.upperFirst(_.camelCase(file.split('.')[0]))] = orm["import"](path.join(modelPath, file));
        });

        /* create associations between models */
        _.each(this, (model) =>{
            if (_.has(model, 'associate')){
                model.associate(this);
            }
        });

        callback();
    }

    activeConnectionsCount() {
        return activeConnections;
    }
};

