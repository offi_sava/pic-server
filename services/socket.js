'use strict';

const  _ = require('lodash'),
    redis = require('redis'),
    config = require('../config'),
    log = require('loginator').getLogger('services/socket'),
    express = require('express'),
    path = require('path'),
    //socketio = require('socket.io'),
    ApiError = require('../lib/errors').ApiError;

const words =

module.exports = class Socket {

    static dependencies() {
        return ['room']
    }

    /**
     * Initialize
     */
    initialize(callback) {

        log.info('Initializing...');
        // let port = process.env.PORT || 3002;

        const server = require("http").createServer();
        const io = require("socket.io")(server, {
            // ...
        });

        io.on('connection', socket => {
            console.log(socket.id);
            // setInterval(() => {
            //     socket.emit('bla', 'dfgdfgdfgsdgdf');
            // }, 5000);

            socket.on('join', name => {
                //socket.join(name);
                log.info(name || socket.username,  'has joined. ID: ' + socket.id);
                //io.emit('message', {msg: message.text, user: socket.username, createdAt: new Date()});
            });
            // submit drawing on canvas to other clients
            socket.on('draw', obj => {
                socket.broadcast.emit('draw', obj);
                socket.broadcast.emit('draw', obj);

                //socket.broadcast.emit('draw', obj);
            });

            socket.on('startDrawing', obj => {
                socket.broadcast.emit('startDrawing', obj);
               // socket.broadcast.emit('startDrawing', obj);
            });

            socket.on('endDrawing', roomId => {
                socket.broadcast.emit('endDrawing', roomId);
                //socket.broadcast.emit('endDrawing');
            });

            socket.on('clearScreen', roomId => {
                socket.broadcast.emit('clearScreen', roomId);
                //socket.broadcast.emit('endDrawing');
            });

            socket.on('deleteRoom', (opts) => {
                socket.broadcast.emit('deleteRoom', opts);
                //socket.broadcast.emit('endDrawing');
            });
            socket.on('changeMasterOfRoom', opts => {
                this.room.selectRandomMasterInRoom(opts)
                    .then(result => {
                        socket.broadcast.emit('changeMasterOfRoom', _.merge(result, opts))
                    });
                //socket.emit('deleteRoom', opts);
                //socket.broadcast.emit('endDrawing');
            });
            socket.on('changedWord', (opts) => {
                socket.broadcast.emit('changedWord', opts);
                //socket.broadcast.emit('endDrawing');
            });

        });



        // io.on('connection', (socket) => {
        //    log.info(socket.id)
        //        //socket.join("room1");
        //     socket.on('hello', (arg) => {
        //         log.info(arg); // world
        //     });
        //     io.emit('lol', {ii: 99})
            // socket.on('connect', function() {
            //     log.info(socket.id);
            //     io.emit('users-changed', {user: socket.username, event: 'left'});
            // });
            //
            // socket.on('set-name', (name) => {
            //     log.info('ujjj')
            //     socket.username = name;
            //     io.emit('users-changed', {user: name, event: 'joined'});
            // });
            //
            // socket.on('send-message', (message) => {
            //     io.emit('message', {msg: message.text, user: socket.username, createdAt: new Date()});
            // });
            //
            //

        //});

        let port = process.env.PORT || 3002;

        server.listen(port, () => {
            log.info('Socket listening in http://localhost:' + port);
        });

        callback();
    }




}
