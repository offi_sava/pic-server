'use strict';

// const crypto = require('crypto'),
//     config =require('../config'),
//     hash = require('../lib/hash');


module.exports = function(orm, dataTypes) {
    const User = orm.define('user', {

        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
            field: 'id'
        },

        name: {
            type: dataTypes.STRING,
            field: 'name'
        },

        roomName: {
            type: dataTypes.STRING,
            field: 'room_name'
        },

        roomId: {
            type: dataTypes.INTEGER,
            field: 'room_id'
        },

        point: {
            type: dataTypes.GEOMETRY('POINT'),
            allowNull: false
        },
        entityIds: {
            type: dataTypes.ARRAY(dataTypes.JSON),
            allowNull: true,
            field: 'entityIds'
        },

        createdAt: {
            type: dataTypes.DATE,
            field: 'created_at',
            defaultValue: orm.fn('NOW')
        },

        updatedAt: {
            type: dataTypes.DATE,
            field: 'updated_at',
            defaultValue: orm.fn('NOW')
        }

    }, {
        //underscored: true,
        timestamps: true,
    });

    return User;
};
