"use strict";

var log = require('loginator').getLogger('logging-filter');

module.exports = {

    /**
     * Logs all accesses to system log
     */
    accessLogFilter: function(req, res, next) {
        next();
        log.debug(req.headers['x-real-ip'] || req.connection.remoteAddress, '-', req.method, req.baseUrl + req.path, '-', res.statusCode);
    }
};



