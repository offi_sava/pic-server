"use strict";

var _ = require('lodash'),
    config = require('../config'),
    ApiError = require('./errors').ApiError,
    log = require('loginator').getLogger('lib/error-filter');

module.exports = function(err) {

    // if ORM or database error
    if ( err.name && _.includes(err.name, 'Sequelize')) {
        err = new ApiError('problem', err.message, config.debug ? err : null);
    }
    return err;
};
