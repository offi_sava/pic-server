'use strict';

const async = require('async');
const util = require('util');
const _ = require('lodash');
const fs = require('fs').promises;
const promisify = require('util').promisify;

module.exports = class Injector {

    constructor() {
        this.initDepts = [];
        this.deps = {};
        this.context = {};
    }

    async init() {
        let path = __dirname + '/../services/'
        let files = await fs.readdir(path);
        for (const f of files) {
            let stat = await fs.lstat(path + f)
            if (stat.isFile()) {
                this.initDepts.push(require(path + f))
            }
        }

        return this;
    }

    async register(dependencies) {

        if (!_.isArray(dependencies)) {
            throw Error('Registering dependencies must be in format [Class1, Class2, ...]');
        }

        dependencies = dependencies.concat(this.initDepts);

        for (const Dep of dependencies) {

            let dep = new Dep();

            if (!this.context[_.lowerFirst(Dep.name)]) {
                if (!_.isFunction(dep.initialize)) {
                    throw Error(`Dependency ${Dep.name} has no initialize method`);
                }

                this.context[_.lowerFirst(Dep.name)] = {
                    $$obj: dep,
                    deps:  _.isFunction(Dep.dependencies) ? Dep.dependencies() : []
                };

                await promisify((...args) => {dep.initialize(...args)})();
            }
        }

        _.each(this.context, (d, name) => {
            _.each(d.deps, dd => {

                if (dd === 'context') {
                    d.$$obj[dd] = this.context
                }

                if (!_.isEmpty(this.context[dd])) {
                    d.$$obj[dd] = !_.has(this.context[dd], '$$obj') ? this.context[dd] : this.context[dd].$$obj
                }
            });
            this.context[name] = d.$$obj
        });

        this.context.getBean = (name) => {
            if (_.has(this.context, name)) {
                return this.context[name];
            } else {
                return {};
            }
        };

        _.each(this.context, (d) => {
            if (_.isFunction(d.ready)) {
                d.ready();
            }
        });

        return this.context;
    }

};
