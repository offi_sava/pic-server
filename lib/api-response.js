'use strict';

var _ = require('lodash'),
    ApiError = require('./errors').ApiError;


/* format ok response object */

var okResponse = function(obj, paging) {
    return {
        'meta': {
            'status': 'ok',
            'error': null,
            'paging': paging
        },
        'response': obj || null
    };
};


/* format error response object, */
/* depending on kind of error    */

var errResponse = function(err) {

    err = require('./error-filter')(err);

    // Creating appropriate err object
    if (err instanceof ApiError) {

        // nothing, that's ok

    } else if (typeof(err) === 'string') {
        err = {
           type: 'unknown',
           message: err
        };
    } else if (err instanceof Error) {
       err = {
           type: 'unknown',
           message: err.message || err,
           cause:err
       };
    }

    return {
        'meta': {
            'status': 'error',
            'error': {
                'type' : err.type || null,
                'message': err.message || null,
                'cause': err.cause || null
            }
        },
        'response': null
    };
};



/* exports */

module.exports = {

    errResponse: errResponse,

    okResponse: okResponse,

    makeResponse: function(err, obj, pagination) {
        return err ? errResponse(err) : okResponse(obj, pagination);
    }
};

