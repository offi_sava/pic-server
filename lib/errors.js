"use strict";
/* generic error */

var ApiError = function(type, msg, cause) {

    // if (!errorTypes[type]) {
    //     log.info("Not standard error type: ", type);
    // }

    this.type = type;
    this.message = msg || errorTypes[type];
    this.cause = cause || {};
};
ApiError.prototype = new Error();


/* api error types */

var errorTypes = {
    'malformed_request' : 'Request is malformed',
    'forbidden'         : 'No enough permissions to access this resource',
    'object_not_found'  : 'Object not found',
    'object_exists'     : 'Object already exists',
    'problem'           : 'Performed action caused internal problem',
    'unimplemented'     : 'Action or resource is not yet implemented',
    'unknown'           : 'Unknown problem',
};


/* exports */

module.exports = {
    ApiError : ApiError,
    errorTypes : errorTypes
};

