var _ = require('lodash'),
    getConfig = require('config-getter').getConfig;

/* module exports */
module.exports = getConfig(__dirname + '/default.js');
