module.exports = {

    /* ----------------------------------- Logging ------------------------------------------ */
    "loginator": {
        "level": "DEBUG",
        "formatter": {
            "type": "text",
            "options": {
                "pattern": "%dtime [%level] (%name) (%location) : %message"
            }
        },
        "appenders": [
            {
                "type": "stdout"
            },
            {
                "type": "file",
                "options": {
                    "path": "$(logDir)/app-server2.log",
                    "rollingSize": "100mb"
                }
            }
        ]
    },
    "logDir": "/tmp",



    /* -------------------------------- Redis credentials ----------------------------------- */
    "redis": {
        "host": "localhost",
        "port": 6379,
        "options": {}
    },

    "database" : {
        "connectionString": "postgres://sava:alexandraqtv@localhost:5432/uroom",
        "pool": {
            "max": 40,
            "min": 0,
            "idle": 30000,
            "acquire": 30000,
            "evict": 1000
        }
    },

    "rooms": {
        "ttl": 60 * 60 * 24 * 30
    },

    "users": {
        "ttl": 60 * 60 * 24 * 30
    }
}
