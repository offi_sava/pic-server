'use strict';

const dbdiff = require('dbdiff'),
    //config = require('../server/config'),
    moment = require('moment'),
    fs = require('fs');


let targetDb = process.argv[2];

if (!targetDb) {
    console.log('You should specify db connection string of db that you want to compare');
    process.exit(1);
}

let currentSchema = fs.readFileSync(__dirname + '/../dbdata/currentState.json');

if (!currentSchema) {
    console.log('Migrations not init. you should run "npm run migrate:init" before this task');
    process.exit(1);
}

currentSchema = JSON.parse(currentSchema);

dbdiff.describeDatabase(targetDb).then((targetSchema) => {

    let dd = new dbdiff.DbDiff();

    let result = {
        UP: null,
        DOWN: null
    };

    dd.compareSchemas(targetSchema, currentSchema);
    result = dd.commands('drop');

    let name = moment().format('DD-MM-YYYY_hh:mm:ss');
    fs.writeFileSync(__dirname + '/../dbdata/diffs/' + name + '-diff.sql', result);

    console.log('Diff file created');
    process.exit(0);
});
