'use strict';

const promisify = require('util').promisify,
      Injector = require('../lib/injector'),
      fs = require('fs'),
      _ = require('lodash');

process.env['ID_SERVER_CONFIG'] = __dirname + '/../locals/config.json';
process.env['NODE_PATH'] = __dirname + '/../server';

const dirDb = [require('../services/db')];
const dirInitial = __dirname + '/../dbdata/initial-data.sql';
const dirCountries = __dirname + '/../dbdata/countries_data.sql';

let inj = new Injector();
let readFile = promisify(fs.readFile);

let isInit = _.includes(JSON.parse(process.env.npm_config_argv).remain, 'init');
let isForce = _.includes(JSON.parse(process.env.npm_config_argv).remain, 'force');

console.log(isForce);

(async function() {
   let app =  await inj.register(dirDb);
   let db = app.getBean('db');

   if (db.orm.options.host !== 'localhost' && !isForce) {
        throw new Error('!!!!!!!!!! You must run this script with "force" argument if you want to sync db server different from localhost');
   }

    await db.orm.sync({force: true});
    console.log('All tables created');

    if(isInit) {

        let data = await readFile(dirInitial, 'utf8');
        await db.orm.query(data);
        console.log('Services inserted');

        data = await readFile(dirCountries, 'utf8');
        await db.orm.query(data);
        console.log('Countries inserted');
    }

    process.exit(0);

})().catch(e => {
    console.log(e.message);
    process.exit(1);

});
