'use strict';

const exec = require('child_process').exec,
    config = require('../server/config').database.connectionString.split('//')[1];

var pgUser = config.split('@')[0].split(':')[0],
    pgPassword = config.split('@')[0].split(':')[1],
    pgDbName = config.split('/')[1],
    output = __dirname + '/../dbdata/schema.sql';

exec(
    'PGPASSWORD=' + pgPassword + ' pg_dump -s -h localhost -d ' + pgDbName + ' -U' + pgUser + ' > ' + output,
    function(error, stdout, stderr) {
        if (error) {
            console.log(error, stderr, stdout);
            process.exit(1);
        }
        console.log('Database ' + pgDbName + ' was dumped in file ' + output);
        process.exit(0)
    });