




const Injector = require('./lib/injector'),
    loginator = require('loginator'),
    config = require('./config');
loginator.configure(config.loginator);
let log = loginator.getLogger('app');

(async () => {
    try {
        let i = await new Injector().init();
        let ctx = await i.register([
            require('./rest')
        ]);

        await ctx.getBean('rest').prepare();
        log.info("Rest app initialized!");
    } catch (err) {
        log.error(err.stack || err)
    }
})()


